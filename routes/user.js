const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth"); 


// Route checking if the user's email already exists in the database
// endpoint: localhost:4000/users/checkemail
router.post("/checkEmail", (req, res) => {
    userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for registering a user
router.post("/register", (req,res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user authentication
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user details retrieval
// router.get("/details", (req, res) => {
//     userController.retrieveUserDetails(req.body).then(resultFromController => res.send(resultFromController));
// })
router.get("/details", auth.verify, (req, res) => {
    
    const userData = auth.decode(req.headers.authorization)
    // Provides the user's ID for the getProfile controller method
    userController.retrieveUserDetails({userId: userData.id}).then(resultFromController => res.send(resultFromController));
})

// Route to enroll user to a course
router.post("/enroll", (req, res) => {
    let data = {
        userId : req.body.userId,
        courseId: req.body.courseId
    }
    userController.enroll(data).then(resultFromController => res.send (
        resultFromController));
})

module.exports = router;