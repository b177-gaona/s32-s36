// imports the User model
const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");
// imports the Course model
const Course = require("../models/Course");

// Controller function for checking email duplicates
module.exports.checkEmailExists = (reqBody) => {
    return User.find({email : reqBody.email}).then(result => {
        if(result.length > 0) {
            return true;
        }
        else {
            return false;
        }
    })
}

// Controller function for user registration
module.exports.registerUser = (reqBody) => {
    // Creates variable "newUser" & instantiates a new "User" object using the mongoose model
    let newUser = new User ({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNumber: reqBody.mobileNumber,
        // change password: reqBody.password
        // where 10 = salt rounds (encryption rounds)
        password : bcrypt.hashSync(reqBody.password, 10)
    })
    // Saves the created object to our database
    return newUser.save().then((user, error) => {
        // User registration failed
        if(error){
            return false;
        }
        // User registration successful
        else {
            return true;
        }
    })
}

// User authentication (/login)
module.exports.loginUser = (reqBody) => {
    return User.findOne({email : reqBody.email}).then(result => {
        // User does not exist
        if (result == null) {
            return false;
        }
        // User exists
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                // Generate an access token
                return { access : auth.createAccessToken(result)};
            }
            // Passwords do not match
            else {
                return false;
            }
        }
    })
}

// Controller function for user details retrieval
// module.exports.retrieveUserDetails = (reqBody) => {
    // return User.findById(reqBody._id).then(result => {
module.exports.retrieveUserDetails = (user) => {
    return User.findById(user.userId).then(result => {
        // User does not exist
        if (result == null) {
            return false;
        }
        // User exists
        else{
            result.password = "";
            return result;
        }
    })
}

// Controller for enrolling the user to a specific course
module.exports.enroll = async (data) => {
    let isUserUpdated = await User.findById(data.userId).then(user => {
        // Adds the courseId in the user's enrollments array
        user.enrollments.push({courseId : data.courseId});

        // Save the updated user information in the database
        return user.save().then((user, error) => {
            if(error) {
                return false;
            }
            else {
                return true;
            }
        })
    })
    // Add the user Id in the enrollees array of the course
    let isCourseUpdated = await Course.findById(data.courseId).then(course => {
        // Adds the userId in the course's enrollees array
        course.enrollees.push({userId : data.userId});

        // Saves the updated course information in the db
        return course.save().then((course, error) => {
            if (error) {
                return false;
            }
            else {
                return true;
            }
        })
    })

    // Condition that will check if the user & course documents have been updated 
    // User enrollment is successful
    if (isUserUpdated && isCourseUpdated) {
        return true;
    }
    // User enrollment failure
    else {
        return false;
    }
}